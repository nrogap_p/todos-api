require 'rails_helper'

RSpec.describe Item, type: :model do
  # Association test
  # ensure Todo model has a 1:m relationship with the Item model
  it { should belong_to(:todo) }
  # Validation tests
  # ensure column title and created_by are present before saving
  it { should validate_presence_of(:name) }
end
